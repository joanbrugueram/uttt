<?php
require('dbConnect.php');

// WARNING: Note that we ONLY need to call mysqli_real_escape_string on $roomId and $message,
//          the only variables passed directly in the MySQL query!

// Fetch and validate basic parameters
isset($_GET["roomId"]) or die("Need roomId");
isset($_GET["type"]) or die("Need type");
isset($_GET["whichPlayer"]) or die("Need whichPlayer");
($_GET["whichPlayer"] == "cross" || $_GET["whichPlayer"] == "circle") or die("Invalid whichPlayer");

$roomId = mysqli_real_escape_string($db, $_GET["roomId"]);
$type = $_GET["type"];
$whichPlayer = $_GET["whichPlayer"];

// Generate message based on parameters
if ($type == "joinedGame" || $type == "keepAlive" || $type == "leaveGame") {

	$message = array(
		"type" => $type,
		"whichPlayer" => $whichPlayer
	);

	// Hacky, but remove the game from the open room list if we got leaveGame
	// This reduces the problem of the user getting a "insta-join,insta-quit" on a room
	if ($type == "leaveGame") {
		$query = "DELETE FROM `uttt_openrooms` WHERE `roomId`='$roomId'";
		mysqli_query($db, $query) or die(mysqli_error());
	}
} else if ($type == "sendChatMessage") {
	isset($_GET["text"]) or die("Need text");

	$message = array(
		"type" => $type,
		"whichPlayer" => $whichPlayer,
		"text" => htmlspecialchars($_GET["text"])
	);
} else if ($type == "placeMark") {
	// Fetch and validate mark parameters
	isset($_GET["whichPlayer"]) or die("Need whichPlayer");
	($_GET["whichPlayer"] == "cross" || $_GET["whichPlayer"] == "circle") or die("Invalid whichPlayer");
	$whichPlayer = $_GET["whichPlayer"];

	foreach (array("subBoardX", "subBoardY", "cellX", "cellY") as $field) {
		isset($_GET[$field]) or die("Need $field");
		is_numeric($_GET[$field]) or die("$field not numeric");
		$$field = (int)$_GET[$field];
		($$field >= 0 && $$field < 3) or die("Invalid $field");
	}

	$message  = array(
		"type" => $type,
		"whichPlayer" => $whichPlayer,
		"subBoardX" => $subBoardX,
		"subBoardY" => $subBoardY,
		"cellX" => $cellX,
		"cellY" => $cellY
	);
} else {
	die("Invalid type");
}

// Insert into DB as JSON
$message = serialize($message);
$message = mysqli_real_escape_string($db, $message);

$query = "INSERT INTO `uttt_messages`(`roomId`, `date`, `message`) VALUES('$roomId', now(), '$message')";
$result = mysqli_query($db, $query) or die(mysqli_error($db));
?>