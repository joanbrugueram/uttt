Ultimate Tic-Tac-Toe
====================
An implementation for a web browser (HTML5+Javascript on the client side, PHP+MySQL on the server side) of the [Ultimate Tic-Tac-Toe](http://mathwithbaddrawings.com/2013/06/16/ultimate-tic-tac-toe/) game.

All modern browsers and Internet Explorer 8+ are supported.

You can see a live demo [here](http://lenstra2.hol.es/uttt/play.html).

TODO list and future ideas
--------------------------
* Add more gameplay modes, e.g. play vs computer, play against friend.

* Add server side cheating checks.

* Automatically switch turn if opponent doesn't play.