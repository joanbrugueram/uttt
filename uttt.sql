-- phpMyAdmin SQL Dump
-- version 4.0.4.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Erstellungszeit: 01. Jul 2013 um 22:29
-- Server Version: 5.5.31-MariaDB-log
-- PHP-Version: 5.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Datenbank: `uttt`
--
CREATE DATABASE IF NOT EXISTS `uttt` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `uttt`;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `uttt_messages`
--

CREATE TABLE IF NOT EXISTS `uttt_messages` (
  `roomId` varchar(30) NOT NULL,
  `messageId` int(11) NOT NULL AUTO_INCREMENT,
  `date` datetime NOT NULL,
  `message` blob NOT NULL,
  PRIMARY KEY (`messageId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=170 ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `uttt_openrooms`
--

CREATE TABLE IF NOT EXISTS `uttt_openrooms` (
  `roomId` varchar(30) NOT NULL,
  `date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
