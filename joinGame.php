<?php
require('dbConnect.php');

function generateRoomId() {
	$base64chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
	return substr(str_shuffle($base64chars), 0, 30);
}

// Delete old rooms and messages. This should prevent assigning dead rooms.
$query = "DELETE FROM `uttt_openrooms` WHERE `date`<DATE_SUB(now(),INTERVAL 40 SECOND)";
mysqli_query($db, $query) or die(mysqli_error($db));
$query = "DELETE FROM `uttt_messages` WHERE `date`<DATE_SUB(now(),INTERVAL 40 SECOND)";
mysqli_query($db, $query) or die(mysqli_error($db));

// Look for existing rooms
mysqli_query($db, "START TRANSACTION") or die(mysqli_error($db));
$result = mysqli_query($db, "SELECT * FROM uttt_openrooms LIMIT 1") or die(mysqli_error($db));

if (mysqli_num_rows($result) != 0) { // Game room available
	$whichPlayerAmI = "cross";

	// Get the room ID
	$room = mysqli_fetch_assoc($result);
	$roomId = $room["roomId"];

	// Delete the room from the list
	$query = "DELETE FROM `uttt_openrooms` WHERE `roomId`='$roomId'";
	mysqli_query($db, $query) or die(mysqli_error($db));

} else {
	$whichPlayerAmI = "circle";

	// Create a new room
	$roomId = generateRoomId();
	$query = "INSERT INTO `uttt_openrooms`(`roomId`, `date`) VALUES('$roomId', now())";
	mysqli_query($db, $query) or die(mysqli_error($db));
}
mysqli_query($db, "COMMIT") or die(mysqli_error($db));

$message = array(
	"roomId" => $roomId,
	"whichPlayerAmI" => $whichPlayerAmI
);
echo json_encode($message);
?>