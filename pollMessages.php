<?php
require('dbConnect.php');
//date_default_timezone_set("UTC"); // For strtotime

// Fetch basic parameters
isset($_GET["roomId"]) or die("Need roomId");
isset($_GET["lastMessageId"]) or die("Need lastMessageId");
is_numeric($_GET["lastMessageId"]) or die("lastMessageId not numeric");

$roomId = mysqli_real_escape_string($db, $_GET["roomId"]);
$lastMessageId = (int)$_GET["lastMessageId"];

// Get messages from DB
$query = "SELECT * FROM `uttt_messages` WHERE `roomId`='$roomId' AND `messageId`>$lastMessageId ORDER BY `messageId` ASC";
$result = mysqli_query($db, $query) or die(mysqli_error($db));

// Get messages from DB
$list = array();
while ($msg = mysqli_fetch_assoc($result)) {
	$contentblob = $msg["message"];
	$content = unserialize($contentblob);
	$content["messageId"] = (int)$msg["messageId"];
	//$content["date"] = strtotime($msg["date"]);
	$list[] = $content;
}
echo json_encode($list);
?>